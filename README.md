# Terraform DigitalOcean demo

## What is this? 
This is a small demo of Terraform in combination with DigitalOcean. It provides a Ubuntu VM running a webserver. 

## Why should I care? 
I don't even know *if* you should care. Should you? 

## How do I run this stuff? 
Make sure you have Terraform installed. If you don't, visit the [Terraform website](www.terraform.io). 

Copy the `example.terraform.tfvars` file to `terraform.tfvars` and enter your DigitalOcean API key, path to your SSH private key and the RSA fingerprint of its public key. Remember you need to add ther publickey in the DigitalOcean management interface as well. 

After setting up the variables, run `terraform plan` to check if everything is set up correctly, and to preview the changes. If you want to actually build the instance, run `terraform apply`. To clean up, run `terraform destroy`.

## It doesn't work!
Well, that's too bad. You could perhaps, well, try to fix it and send me a pull request. 
