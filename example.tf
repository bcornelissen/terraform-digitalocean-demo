# Configure the DigitalOcean provider
provider "digitalocean" {
  token = "${var.do_token}"
}

# Create Ubuntu server
resource "digitalocean_droplet" "ubuntu1" {
  image = "ubuntu-14-04-x64"
  name  = "ubuntu1"
  region = "ams3"
  size  = "512mb"
  ssh_keys = [ "${var.do_ssh_pubkeys}" ]
  provisioner "file" { 
    source = "ubuntu1.pp"
    destination = "/tmp/ubuntu1.pp"
    connection {
      type = "ssh"
      user = "root"
      key_file = "${var.do_ssh_privkey}"
    }
  }
  provisioner "remote-exec" {
    script = "ubuntu-deploy-puppet.sh"  
    connection {
      type = "ssh"      
      user = "root"
      key_file = "${var.do_ssh_privkey}"
    }
  }

}
